#!/usr/bin/env python
# pylint: disable=missing-docstring

from __future__ import unicode_literals, print_function

import os
import subprocess

from cookiecutter import prompt, utils

PROJECT_DIR = os.path.realpath(os.getcwd())


def create_git_repo():
    commands = [
        ['git', 'init'],
        ['git', 'add', '.'],
        ['git', 'commit', '-m', 'Repository created'],
    ]
    for cmd in commands:
        subprocess.check_call(cmd)


def main():
    # Create here to avoid storing .keep file.
    utils.make_sure_path_exists('packages')

    if prompt.read_user_yes_no('Init Git repository? (y/n)', None):
        create_git_repo()

    print('Congratulations! Your repository created in ' + PROJECT_DIR)


if __name__ == '__main__':
    main()
